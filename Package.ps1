﻿cd putdig-common
python setup.py install -f
cd ..
rmdir -r putdig-common
pip install -U -r dev-requirements.txt

pyinstaller --onefile --version-file windows_version_info_pumqry.txt -n PumQry putdig_cli/pumqry.py
pyinstaller --onefile --version-file windows_version_info_pumgen.txt -n PumGen putdig_cli/pumgen.py
pyinstaller --onefile --version-file windows_version_info_puminj.txt -n PumInj putdig_cli/puminj.py
