.. Pumpkin Sphinx Documentation documentation master file, created by
   sphinx-quickstart on Fri Nov  8 09:53:43 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PuTDIG-CLI's documentation!
========================================================

.. toctree::
    :maxdepth: 2
    :caption: PumQry:

    PumQry<pages/pumqry/pumqry>
    API<pages/pumqry/api>

.. toctree::
    :maxdepth: 2
    :caption: PumGen:

    PumGen<pages/pumgen/pumgen>
    Interactive Mode<pages/pumgen/modes/interactive>
    API<pages/pumgen/api>

.. toctree::
    :maxdepth: 2
    :caption: PumInj:

    PumInj<pages/puminj/puminj>
    API<pages/puminj/api>

PutDIG-CLI includes three programs intended to help discover, generate, and
inject simulated telemetry data into one or more SupMCU modules:
 - :doc:`PumQry<pages/pumqry/pumqry>` can be used to discover SupMCU modules on a variety of I2C platforms and read their telemetry.
 - :doc:`PumGen<pages/pumgen/pumgen>` can be used to generate and edit telemetry data based on a definition obtained from :doc:`PumQry<pages/pumqry/pumqry>`, both manually and by using nominal or abnormal telemetry.
 - :doc:`PumInj<pages/puminj/puminj>` can be used to inject telemetry data into one or more SupMCU modules through a variety of I2C platforms or a serial connection.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
