PumGen CLI Usage
================

The PumGen CLI program can generate and edit telemetry date in three modes: interactive, manual, and automatic.

Basics
******

.. highlight:: None
::

    A command line program to generate and edit telemetry

    Usage:
        pumgen.py [SWITCHES]

    Editing:
        -m, --module VALUE:str           What module to edit the data for; requires --telemetry, --value
        -n, --item VALUE:int             the number of the item to edit, if you are only editing one value of a telemetry object with more than one; requires --module,
                                         --telemetry, --value
        -t, --telemetry VALUE:str        What telemetry to edit from the slected module; requires --module, --value
        -v, --value VALUE:str            The value to set the telemetry item to.; requires --module, --telemetry

    Meta-switches:
        -h, --help                       Prints this help message and quits
        --help-all                       Prints help messages of all sub-commands and quits
        --version                        Prints the program's version and quits

    Switches:
        -d, --input-file VALUE:str       The definiton file to pattern the telemetry data after or a telemetry file to edit a copy of; required
        -f, --output-file VALUE:str      The file to save JSON data to
        -i, --interactive                Starts an interactive session for manually setting telemetry values; excludes --pretty, --quiet, --module
        -p, --pretty                     Format the JSON output; excludes --interactive
        -q, --quiet                      Runs without outputing anything to stdout; requires --output-file; excludes --interactive


Input File
^^^^^^^^^^
The input file is the only flag that is required no matter what mode you are
using.  :doc:`PumGen<pumgen>` will accept two kinds of JSON files as inputs:
telemetry files created by this program.  If a telemetry definition file is
telemetry definition files, like those created by :doc:`pumqry</pages/pumqry/pumqry>`, and
inputted, :doc:`PumGen<pumgen>` will generate telemetry data with all values
initialized as their defaults (mainly 0's and empty strings), and allow you to
edit these values.  If a telemetry file is inputted, :doc:`PumGen<pumgen>` will
not modify that file, but it will load all of its data for editing.

Output File
^^^^^^^^^^^
The output file is an optional argument allowing you to save the edited
telemetry data to a file in JSON format.  If not included and not in interactive
mode, the JSON output will be printed to :code:`stdout`.  In interactive mode, there is
an option to select a filename to save as when this option is unset.

.. note::

    The output file argument doesn't stop terminal output, so the program will
    still print out information unless you use the -q flag.

Interactive
^^^^^^^^^^^
The interactive flag starts the :doc:`interactive mode<modes/interactive>` and the only other flags that
can be combined with it are the input and output file flags.  See more about the
interactive mode in :doc:`interactive<modes/interactive>`

Module
^^^^^^
The module flag is the first flag needed to edit a value in manual mode.  It
selects which SupMCU module you want to edit the telemetry data for.  A
:code:`ValueError` will be raised if the module isn't found in the input file.

Telemetry
^^^^^^^^^
The telemetry flag selects which telemetry object to edit the/a value for.  Like
the :code:`--module` flag, if the named telemetry object isn't found in the
selected module, a :code:`ValueError` will be raised.

Item
^^^^
Many telemetry objects have more than one value to edit.  The item flag allows
you to select which one to edit by passing in the index of the item to edit.
This flag isn't required for telemetry objects that only have one telemetry
item.  A :code:`ValueError` is raised whenever there is more than one item and
this flag isn't included or an index that is higher than the maximum is passed in.

Value
^^^^^
The value flag is where the user provides the value to set the telemetry item
to.  The value will be checked to make sure that it is the correct type for the
telemetry item that is being edited and that it is withing the allowed range of
values.

Pretty
^^^^^^
Outside of :doc:`interactive mode<modes/interactive>`, the JSON data is printed
to :code:`stdout`.  This flag formats the JSON, so that it is more readable.

Quiet
^^^^^
The quiet flag prevents output from being printed when using the
:code:`--output-file` flag and not in :doc:`interactive mode<modes/interactive>`.

Examples
********

Creating JSON telemetry data out of a single-module telemetry definition file.

.. container:: toggle

    .. container:: header

        .. code-block:: bash

            $ pumgen.py -d telemetry-def.json

    .. code-block:: json

        [
            {
                "name": "BIM",
                "cmd_name": "BIM",
                "address": 82,
                "supmcu_telemetry": [
                    {
                        "idx": 0,
                        "name": "Firmware version",
                        "size": 77,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 1,
                                    "value": "",
                                    "string_value": ""
                                }
                            ]
                        }
                    },
                    {
                        "idx": 1,
                        "name": "SCPI cmds processed",
                        "size": 21,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 9,
                                    "value": 0,
                                    "string_value": "0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 2,
                        "name": "SCPI errs processed",
                        "size": 21,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 9,
                                    "value": 0,
                                    "string_value": "0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 3,
                        "name": "Voltage status (TBD)",
                        "size": 21,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 13,
                                    "value": 0,
                                    "string_value": "0x0"
                                },
                                {
                                    "data_type": 13,
                                    "value": 0,
                                    "string_value": "0x0"
                                },
                                {
                                    "data_type": 13,
                                    "value": 0,
                                    "string_value": "0x0"
                                },
                                {
                                    "data_type": 13,
                                    "value": 0,
                                    "string_value": "0x0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 4,
                        "name": "SupMCU CPU self tests",
                        "size": 35,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 9,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 9,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 5,
                        "name": "Elapsed time in seconds",
                        "size": 21,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 9,
                                    "value": 0,
                                    "string_value": "0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 6,
                        "name": "Elapsed context switches",
                        "size": 21,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 9,
                                    "value": 0,
                                    "string_value": "0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 7,
                        "name": "Elapsed idling hooks",
                        "size": 21,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 9,
                                    "value": 0,
                                    "string_value": "0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 8,
                        "name": "MCU load [%]",
                        "size": 17,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 11,
                                    "value": 0.0,
                                    "string_value": "0.0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 9,
                        "name": "Module Serial Number",
                        "size": 15,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 10,
                        "name": "Module I2C Address",
                        "size": 14,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 13,
                                    "value": 0,
                                    "string_value": "0x0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 11,
                        "name": "Oscillator Tuning Value",
                        "size": 14,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 4,
                                    "value": 0,
                                    "string_value": "0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 12,
                        "name": "Number of NVM Write cycles",
                        "size": 15,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 6,
                                    "value": 0,
                                    "string_value": "0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 13,
                        "name": "Last Processor Reset",
                        "size": 15,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 6,
                                    "value": 0,
                                    "string_value": "0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 14,
                        "name": "Number Telem. Item [sup,mod]",
                        "size": 17,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 15,
                        "name": "SupMCU (CTMU) Temp [0.1K]",
                        "size": 17,
                        "type": 0,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                }
                            ]
                        }
                    }
                ],
                "module_telemetry": [
                    {
                        "idx": 0,
                        "name": "Temperature [0.1 K]",
                        "size": 25,
                        "type": 1,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 1,
                        "name": "UART Status",
                        "size": 19,
                        "type": 1,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 5,
                                    "value": 0,
                                    "string_value": "0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 2,
                        "name": "IMU Data",
                        "size": 25,
                        "type": 1,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 8,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 8,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 8,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 8,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 8,
                                    "value": 0,
                                    "string_value": "0"
                                },
                                {
                                    "data_type": 8,
                                    "value": 0,
                                    "string_value": "0"
                                }
                            ]
                        }
                    },
                    {
                        "idx": 3,
                        "name": "Pin Puller Status",
                        "size": 14,
                        "type": 1,
                        "sup_telemetry": {
                            "header": {
                                "ready": true,
                                "timestamp": -1
                            },
                            "items": [
                                {
                                    "data_type": 13,
                                    "value": 0,
                                    "string_value": "0x0"
                                }
                            ]
                        }
                    }
                ]
            }
        ]

\

Starting an interactive session using a telemetry (not telemetry definition) file.

.. container:: toggle

    .. container:: header

        .. code-block:: bash

            $ pumgen.py -d telemetry.json -i

    .. code-block:: none

        Welcome to pumgen, please select a module to set telemetry for:

        1.  BIM
        2.  PIM
        3.  DCPS
        4.  RHM
        5.  DASA

        B. Back
        Q. Quit and save
        Ctrl-C to quit without saving

        Choice:

\

Editing a single value in a telemetry file using the manual mode.  This method
of editing telemetry data is best suited for using in scripts.  Notice how, in
order to edit one file, the same file name must be used as both an input and an
output.  Since :code:`-q` is used, there is no terminal output

.. code-block:: none

    $ pumgen.py -d telemetry.json -f telemetry.json -q -m PIM -t "Oscillator Tuning Value" -v 7
