PumInj CLI Usage
================

The PumInj CLI program can inject telemetry data into a SupMCU module through a variety of I2C interfaces or a serial port.

Basics
******

.. highlight:: None
::

    A command line program that takes in a set of flags and injects the given telemetry into a SupMCU module.

    Usage:
        puminj.py [SWITCHES]

    Meta-switches:
        -h, --help                                  Prints this help message and quits
        --help-all                                  Prints help messages of all sub-commands and quits
        -v, --version                               Prints the program's version and quits

    Switches:
        -g, --generated-set VALUE:ExistingFile      The JSON file of generated telemetry to inject; required
        -i, --ignore                                If set, PumInj will ignore any extra modules in the file that are not found on the I2C bus
        -p, --port VALUE:str                        COM# (Windows), device path (Linux, type i2cdriver), or bus # (Linux, type linux) to I2C Master device or comma-separated list of serial ports for type serial;
                                                    required
        -t, --type VALUE:str                        Type of I2C device at -p. Can be i2cdriver, aardvark, linux, or serial, all other values rejected; required

Type
^^^^
Currently, three device types are supported: i2cdriver, linux, and serial.
Any of the supported types can be passed in, case insensitive, but any other
string will raise a :code:`ValueError`.  We are planning to support the Aardvark
type, so that will raise a :code:`NotImplementedError` instead.

Port
^^^^
What is passed in to the port flag depends on the type.  The i2cdriver connects
to your computer via usb, so it requires a serial device.  On windows this is a
:code:`COM#` port such as :code:`COM2` and on linux this is a device path such
as :code:`/dev/ttyUSB2`.  For the :code:`linux` type, it is assumed that the
program is running on the computer connected directly to the SupMCU through I2C,
so it requires a I2C device id.  For example if the SupMCU was connected to the
:code:`/dev/i2c-1` bus, you would pass in "1" to :doc:`PumQry<../pumqry/pumqry>`.
For the :code:`serial` type, a comma-separated list of serial devices is needed.

.. warning::

    Putting spaces between the serial ports for type :code:`serial` will cause
    an error.

Generated Telemetry Set
^^^^^^^^^^^^^^^^^^^^^^^
The telemetry data to inject is provided by a JSON file containing a set of
generated telemetry, such as those created by :doc:`PumGen<../pumgen/pumgen>`,
:doc:`PumQry<../pumqry/pumqry>`, or the PutDig web application

Ignore
^^^^^^
If set, :any:`PumInj` will ignore any telemetry data in the generated telemetry
file that is not found on the I2C bus or serial list.  If not set, an
:code:`IndexError` will be raised.
