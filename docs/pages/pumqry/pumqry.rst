PumQry CLI Usage
================

The PumQry CLI program can print or save JSON telemetry data based on a given device type, port, and optionally address(es).

Basics
******

.. highlight:: None
::

    A command line program that takes in a set of flags then outputs the telemetry definition or data.

    Usage:
        pumqry.py [SWITCHES] addresses...

    Meta-switches:
        -h, --help                Prints this help message and quits
        --help-all                Prints help messages of all sub-commands and quits
        -v, --version             Prints the program's version and quits

    Switches:
        -d, --pretty              Format the JSON output
        -e, --definition          Saves the telemetry as a telemetry definition instead of telemetry data
        -f, --file VALUE:str      The file to save JSON data to
        -l, --list                List all of the available i2c addresses without getting telemetry data; requires --port, --type; excludes --file, --quiet, --pretty
        -p, --port VALUE:str      COM# (Windows), device path (Linux, type i2cdriver), or bus # (Linux, type linux) to I2C Master device; required
        -q, --quiet               Runs without outputing anything to stdout; requires --file
        -t, --type VALUE:str      Type of I2C device at -p. Can be i2cdriver, aardvark, linux, all other values rejected; required

Type
^^^^
Currently, two device types are supported: i2cdriver, and linux. Either name
can be passed in, case insensitive, but any other string will raise a
:code:`ValueError`.  We are planning to support the Aardvark type, so that will
raise a :code:`NotImplementedError` instead.

Port
^^^^
What is passed in to the port flag depends on the type.  The i2cdriver connects
to your computer via usb, so it requires a serial device.  On windows this is a
:code:`COM#` port such as :code:`COM2` and on linux this is a device path such
as :code:`/dev/ttyUSB2`.  For the linux type, it is assumed that the program is
running on the computer connected directly to the SupMCU through I2C, so it
requires a I2C device id.  For example if the SupMCU was connected to the
:code:`/dev/i2c-1` bus, you would pass in "1" to :doc:`PumQry<pumqry>`.

Addresses
^^^^^^^^^
Addresses is an optional, space separated list of i2c addresses in hex that
specifies what i2c addresses to read telemetry data from.  If none are included,
the program will get telemetry data from all the devices connected to the i2c bus.

File
^^^^
File is an optional argument, indicating what filename to save the JSON output
as.

.. note::

    The file argument doesn't redirect the terminal output, so the program will
    still print out information unless you use the -q flag.

Definition
^^^^^^^^^^
This flag allows :doc:`PumQry<pumqry>` to output telemetry definitions instead of
telemetry data.

List
^^^^
If you only want to see the active I2C addresses on the selected port, the list
flag will simply print them out without querying any telemetry data.

Quiet
^^^^^
By default :doc:`PumQry<pumqry>` prints out all of the telemetry data it receives, even
if it is also saving this data to a file.  This flag prevents the program from
printing anything out.

Pretty
^^^^^^
If you wanted to manually examine the JSON telemetry data that was printed out,
it would be difficult because there is no formatting.  The pretty flag fixes
this by formatting the JSON before printing.

Examples
********

Bare minimum command with type "linux" using the :code:`/dev/i2c-1` bus.

.. code-block:: shell

    $ PumQry.py -t linux -p 1
    [{"version": "BIM (on STM) Rev C fw v1.3.0b & SupMCU Core v1.4.1b", "definition": {"name": "BIM", "cmd_name": "BIM", "address": 82, "supmcu_telemetry": {"0": {"name": "Firmware version", "telemetry_length": 77, "idx": 0, "format": "S"}, "1": {"name": "SCPI cmds processed", "telemetry_length": 21, "idx": 1, "format": "l"}, "2": {"name": "SCPI errs processed", "telemetry_length": 21, "idx": 2, "format": "l"}, "3": {"name": "Voltage status (TBD)", "telemetry_length": 21, "idx": 3, "format": "x,x,x,x"}, "4": {"name": "SupMCU CPU self tests", "telemetry_length": 35, "idx": 4, "format": "l,l,s,s,s"}, "5": {"name": "Elapsed time in seconds", "telemetry_length": 21, "idx": 5, "format": "l"}, "6": {"name": "Elapsed context switches", "telemetry_length": 21, "idx": 6, "format": "l"}, "7": {"name": "Elapsed idling hooks", "telemetry_length": 21, "idx": 7, "format": "l"}, "8": {"name": "MCU load [%]", "telemetry_length": 17, "idx": 8, "format": "f"}, "9": {"name": "Module Serial Number", "telemetry_length": 15, "idx": 9, "format": "s"}, "10": {"name": "Module I2C Address", "telemetry_length": 14, "idx": 10, "format": "x"}, "11": {"name": "Oscillator Tuning Value", "telemetry_length": 14, "idx": 11, "format": "t"}, "12": {"name": "Number of NVM Write cycles", "telemetry_length": 15, "idx": 12, "format": "n"}, "13": {"name": "Last Processor Reset", "telemetry_length": 15, "idx": 13, "format": "n"}, "14": {"name": "Number Telem. Item [sup,mod]", "telemetry_length": 17, "idx": 14, "format": "s,s"}, "15": {"name": "SupMCU (CTMU) Temp [0.1K]", "telemetry_length": 17, "idx": 15, "format": "s"}}, "module_telemetry": {"0": {"name": "Temperature [0.1 K]", "telemetry_length": 25, "idx": 0, "format": "s,s,s,s,s,s"}, "1": {"name": "UART Status", "telemetry_length": 19, "idx": 1, "format": "s,s,s"}, "2": {"name": "IMU Data", "telemetry_length": 25, "idx": 2, "format": "d,d,d,d,d,d"}, "3": {"name": "Pin Puller Status", "telemetry_length": 14, "idx": 3, "format": "x"}}}}, {"version": "PIM (on STM) Rev D fw v1.3.0a & SupMCU Core v1.4.1b", "definition": {"name": "PIM", "cmd_name": "PIM", "address": 83, "supmcu_telemetry": {"0": {"name": "Firmware version", "telemetry_length": 77, "idx": 0, "format": "S"}, "1": {"name": "SCPI cmds processed", "telemetry_length": 21, "idx": 1, "format": "l"}, "2": {"name": "SCPI errs processed", "telemetry_length": 21, "idx": 2, "format": "l"}, "3": {"name": "Voltage status (TBD)", "telemetry_length": 21, "idx": 3, "format": "x,x,x,x"}, "4": {"name": "SupMCU CPU self tests", "telemetry_length": 35, "idx": 4, "format": "l,l,s,s,s"}, "5": {"name": "Elapsed time in seconds", "telemetry_length": 21, "idx": 5, "format": "l"}, "6": {"name": "Elapsed context switches", "telemetry_length": 21, "idx": 6, "format": "l"}, "7": {"name": "Elapsed idling hooks", "telemetry_length": 21, "idx": 7, "format": "l"}, "8": {"name": "MCU load [%]", "telemetry_length": 17, "idx": 8, "format": "f"}, "9": {"name": "Module Serial Number", "telemetry_length": 15, "idx": 9, "format": "s"}, "10": {"name": "Module I2C Address", "telemetry_length": 14, "idx": 10, "format": "x"}, "11": {"name": "Oscillator Tuning Value", "telemetry_length": 14, "idx": 11, "format": "t"}, "12": {"name": "Number of NVM Write cycles", "telemetry_length": 15, "idx": 12, "format": "n"}, "13": {"name": "Last Processor Reset", "telemetry_length": 15, "idx": 13, "format": "n"}, "14": {"name": "Number Telem. Item [sup,mod]", "telemetry_length": 17, "idx": 14, "format": "s,s"}, "15": {"name": "SupMCU (CTMU) Temp [0.1K]", "telemetry_length": 17, "idx": 15, "format": "s"}}, "module_telemetry": {"0": {"name": "Payload Currents [mA]", "telemetry_length": 21, "idx": 0, "format": "s,s,s,s"}, "1": {"name": "Payload Shunt [micro Ohm]", "telemetry_length": 21, "idx": 1, "format": "s,s,s,s"}, "2": {"name": "Payload Current limits [mA]", "telemetry_length": 21, "idx": 2, "format": "s,s,s,s"}, "3": {"name": "PIM Status Register", "telemetry_length": 14, "idx": 3, "format": "x"}, "4": {"name": "Payload Overcurrents [mA]", "telemetry_length": 21, "idx": 4, "format": "s,s,s,s"}, "5": {"name": "Payload Voltages [mV]", "telemetry_length": 21, "idx": 5, "format": "s,s,s,s"}}}}, {"version": "DCPS (on STM) Rev B fw v1.7.0b & SupMCU Core v1.4.1b", "definition": {"name": "DCPS", "cmd_name": "DCPS", "address": 84, "supmcu_telemetry": {"0": {"name": "Firmware version", "telemetry_length": 77, "idx": 0, "format": "S"}, "1": {"name": "SCPI cmds processed", "telemetry_length": 21, "idx": 1, "format": "l"}, "2": {"name": "SCPI errs processed", "telemetry_length": 21, "idx": 2, "format": "l"}, "3": {"name": "Voltage status (TBD)", "telemetry_length": 21, "idx": 3, "format": "x,x,x,x"}, "4": {"name": "SupMCU CPU self tests", "telemetry_length": 35, "idx": 4, "format": "l,l,s,s,s"}, "5": {"name": "Elapsed time in seconds", "telemetry_length": 21, "idx": 5, "format": "l"}, "6": {"name": "Elapsed context switches", "telemetry_length": 21, "idx": 6, "format": "l"}, "7": {"name": "Elapsed idling hooks", "telemetry_length": 21, "idx": 7, "format": "l"}, "8": {"name": "MCU load [%]", "telemetry_length": 17, "idx": 8, "format": "f"}, "9": {"name": "Module Serial Number", "telemetry_length": 15, "idx": 9, "format": "s"}, "10": {"name": "Module I2C Address", "telemetry_length": 14, "idx": 10, "format": "x"}, "11": {"name": "Oscillator Tuning Value", "telemetry_length": 14, "idx": 11, "format": "t"}, "12": {"name": "Number of NVM Write cycles", "telemetry_length": 15, "idx": 12, "format": "n"}, "13": {"name": "Last Processor Reset", "telemetry_length": 15, "idx": 13, "format": "n"}, "14": {"name": "Number Telem. Item [sup,mod]", "telemetry_length": 17, "idx": 14, "format": "s,s"}, "15": {"name": "SupMCU (CTMU) Temp [0.1K]", "telemetry_length": 17, "idx": 15, "format": "s"}}, "module_telemetry": {"0": {"name": "SAI1 Converter Data", "telemetry_length": 26, "idx": 0, "format": "s,s,n,n,x,d"}, "1": {"name": "SAI2 Converter Data", "telemetry_length": 26, "idx": 1, "format": "s,s,n,n,x,d"}, "2": {"name": "SAI3 Converter Data", "telemetry_length": 26, "idx": 2, "format": "s,s,n,n,x,d"}, "3": {"name": "SAI4 Converter Data", "telemetry_length": 26, "idx": 3, "format": "s,s,n,n,x,d"}, "4": {"name": "SAI5 Converter Data", "telemetry_length": 26, "idx": 4, "format": "s,s,n,n,x,d"}, "5": {"name": "SAI6 Converter Data", "telemetry_length": 26, "idx": 5, "format": "s,s,n,n,x,d"}, "6": {"name": "BATT1 Converter Data", "telemetry_length": 26, "idx": 6, "format": "s,s,n,n,x,d"}, "7": {"name": "BATT2 Converter Data", "telemetry_length": 26, "idx": 7, "format": "s,s,n,n,x,d"}, "8": {"name": "3V3 Converter Data", "telemetry_length": 26, "idx": 8, "format": "s,s,n,n,x,d"}, "9": {"name": "5V Converter Data", "telemetry_length": 26, "idx": 9, "format": "s,s,n,n,x,d"}, "10": {"name": "12V Converter Data", "telemetry_length": 26, "idx": 10, "format": "s,s,n,n,x,d"}, "11": {"name": "AUX Converter Data", "telemetry_length": 26, "idx": 11, "format": "s,s,n,n,x,d"}, "12": {"name": "VREF1 Converter Data", "telemetry_length": 17, "idx": 12, "format": "s,s"}, "13": {"name": "VREF2 Converter Data", "telemetry_length": 17, "idx": 13, "format": "s,s"}, "14": {"name": "VREF3 Converter Data", "telemetry_length": 17, "idx": 14, "format": "s,s"}, "15": {"name": "VREF4 Converter Data", "telemetry_length": 17, "idx": 15, "format": "s,s"}, "16": {"name": "VREF5 Converter Data", "telemetry_length": 17, "idx": 16, "format": "s,s"}, "17": {"name": "HSK_N5V VRef Data", "telemetry_length": 17, "idx": 17, "format": "s,s"}, "18": {"name": "CORE_1V5 VRef Data", "telemetry_length": 17, "idx": 18, "format": "s,s"}, "19": {"name": "HSK_3V3 VRef Data", "telemetry_length": 17, "idx": 19, "format": "s,s"}, "20": {"name": "HSK_5V VRef Data", "telemetry_length": 17, "idx": 20, "format": "s,s"}, "21": {"name": "HSK_10V VRef Data", "telemetry_length": 17, "idx": 21, "format": "s,s"}, "22": {"name": "RING1 VRef Data", "telemetry_length": 17, "idx": 22, "format": "s,s"}, "23": {"name": "RING2 VRef Data", "telemetry_length": 17, "idx": 23, "format": "s,s"}, "24": {"name": "RING3 VRef Data", "telemetry_length": 17, "idx": 24, "format": "s,s"}, "25": {"name": "SAI1 I/O [100mK]", "telemetry_length": 15, "idx": 25, "format": "n"}, "26": {"name": "SAI2 I/O [100mK]", "telemetry_length": 15, "idx": 26, "format": "n"}, "27": {"name": "SAI3 I/O [100mK]", "telemetry_length": 15, "idx": 27, "format": "n"}, "28": {"name": "SAI4 I/O [100mK]", "telemetry_length": 15, "idx": 28, "format": "n"}, "29": {"name": "SAI5A I/O [100mK]", "telemetry_length": 15, "idx": 29, "format": "n"}, "30": {"name": "SAI5B I/O [100mK]", "telemetry_length": 15, "idx": 30, "format": "n"}, "31": {"name": "SAI6A I/O [100mK]", "telemetry_length": 15, "idx": 31, "format": "n"}, "32": {"name": "SAI6B I/O [100mK]", "telemetry_length": 15, "idx": 32, "format": "n"}, "33": {"name": "SNS VUSB [mV]", "telemetry_length": 15, "idx": 33, "format": "s"}, "34": {"name": "-RESET voltage [mV]", "telemetry_length": 15, "idx": 34, "format": "s"}, "35": {"name": "SNS VREF 4096 mV [mV]", "telemetry_length": 15, "idx": 35, "format": "s"}, "36": {"name": "Read value of register requested", "telemetry_length": 14, "idx": 36, "format": "x"}, "37": {"name": "FGPA Version number.", "telemetry_length": 16, "idx": 37, "format": "c.c.c"}, "38": {"name": "BATT1 Charging Current [mA], Cha", "telemetry_length": 18, "idx": 38, "format": "n,s,u"}, "39": {"name": "BATT2 Charging Current [mA], Cha", "telemetry_length": 18, "idx": 39, "format": "n,s,u"}, "40": {"name": "Register Read Address and Value", "telemetry_length": 17, "idx": 40, "format": "z,z"}, "41": {"name": "Dosimeter [mGy]", "telemetry_length": 15, "idx": 41, "format": "s"}}}}, {"version": "RHM (on STM) Rev A fw v1.0.1b & SupMCU Core v1.4.1b", "definition": {"name": "RHM", "cmd_name": "RHM", "address": 85, "supmcu_telemetry": {"0": {"name": "Firmware version", "telemetry_length": 77, "idx": 0, "format": "S"}, "1": {"name": "SCPI cmds processed", "telemetry_length": 21, "idx": 1, "format": "l"}, "2": {"name": "SCPI errs processed", "telemetry_length": 21, "idx": 2, "format": "l"}, "3": {"name": "Voltage status (TBD)", "telemetry_length": 21, "idx": 3, "format": "x,x,x,x"}, "4": {"name": "SupMCU CPU self tests", "telemetry_length": 35, "idx": 4, "format": "l,l,s,s,s"}, "5": {"name": "Elapsed time in seconds", "telemetry_length": 21, "idx": 5, "format": "l"}, "6": {"name": "Elapsed context switches", "telemetry_length": 21, "idx": 6, "format": "l"}, "7": {"name": "Elapsed idling hooks", "telemetry_length": 21, "idx": 7, "format": "l"}, "8": {"name": "MCU load [%]", "telemetry_length": 17, "idx": 8, "format": "f"}, "9": {"name": "Module Serial Number", "telemetry_length": 15, "idx": 9, "format": "s"}, "10": {"name": "Module I2C Address", "telemetry_length": 14, "idx": 10, "format": "x"}, "11": {"name": "Oscillator Tuning Value", "telemetry_length": 14, "idx": 11, "format": "t"}, "12": {"name": "Number of NVM Write cycles", "telemetry_length": 15, "idx": 12, "format": "n"}, "13": {"name": "Last Processor Reset", "telemetry_length": 15, "idx": 13, "format": "n"}, "14": {"name": "Number Telem. Item [sup,mod]", "telemetry_length": 17, "idx": 14, "format": "s,s"}, "15": {"name": "SupMCU (CTMU) Temp [0.1K]", "telemetry_length": 17, "idx": 15, "format": "s"}}, "module_telemetry": {"0": {"name": "Lithium Radio Power Status", "telemetry_length": 14, "idx": 0, "format": "x"}, "1": {"name": "Lithium Radio UART Status", "telemetry_length": 14, "idx": 1, "format": "u"}, "2": {"name": "Lithium Radio Config Pin Status", "telemetry_length": 14, "idx": 2, "format": "x"}, "3": {"name": "GlobalStar Radio Power Status", "telemetry_length": 14, "idx": 3, "format": "x"}, "4": {"name": "GlobalStar UART Status", "telemetry_length": 14, "idx": 4, "format": "u"}, "5": {"name": "GlobalStar Radio DIN Status", "telemetry_length": 14, "idx": 5, "format": "x"}, "6": {"name": "GlobalStar Radio Busy Status", "telemetry_length": 14, "idx": 6, "format": "x"}, "7": {"name": "Soft WDT period", "telemetry_length": 17, "idx": 7, "format": "i"}, "8": {"name": "GS Transmission Status", "telemetry_length": 14, "idx": 8, "format": "x"}, "9": {"name": "GS Transmission Sent", "telemetry_length": 17, "idx": 9, "format": "i"}, "10": {"name": "GS Transmission Failed", "telemetry_length": 17, "idx": 10, "format": "i"}, "11": {"name": "GS Configuration Status", "telemetry_length": 14, "idx": 11, "format": "u"}, "12": {"name": "GS +VIN", "telemetry_length": 15, "idx": 12, "format": "s"}, "13": {"name": "GS Temperature", "telemetry_length": 15, "idx": 13, "format": "s"}}}}, {"version": "DASA (on STM) Rev A fw v1.4.0a & SupMCU Core v1.4.1b", "definition": {"name": "DASA", "cmd_name": "DASA", "address": 87, "supmcu_telemetry": {"0": {"name": "Firmware version", "telemetry_length": 77, "idx": 0, "format": "S"}, "1": {"name": "SCPI cmds processed", "telemetry_length": 21, "idx": 1, "format": "l"}, "2": {"name": "SCPI errs processed", "telemetry_length": 21, "idx": 2, "format": "l"}, "3": {"name": "Voltage status (TBD)", "telemetry_length": 21, "idx": 3, "format": "x,x,x,x"}, "4": {"name": "SupMCU CPU self tests", "telemetry_length": 35, "idx": 4, "format": "l,l,s,s,s"}, "5": {"name": "Elapsed time in seconds", "telemetry_length": 21, "idx": 5, "format": "l"}, "6": {"name": "Elapsed context switches", "telemetry_length": 21, "idx": 6, "format": "l"}, "7": {"name": "Elapsed idling hooks", "telemetry_length": 21, "idx": 7, "format": "l"}, "8": {"name": "MCU load [%]", "telemetry_length": 17, "idx": 8, "format": "f"}, "9": {"name": "Module Serial Number", "telemetry_length": 15, "idx": 9, "format": "s"}, "10": {"name": "Module I2C Address", "telemetry_length": 14, "idx": 10, "format": "x"}, "11": {"name": "Oscillator Tuning Value", "telemetry_length": 14, "idx": 11, "format": "t"}, "12": {"name": "Number of NVM Write cycles", "telemetry_length": 15, "idx": 12, "format": "n"}, "13": {"name": "Last Processor Reset", "telemetry_length": 15, "idx": 13, "format": "n"}, "14": {"name": "Number Telem. Item [sup,mod]", "telemetry_length": 17, "idx": 14, "format": "s,s"}, "15": {"name": "SupMCU (CTMU) Temp [0.1K]", "telemetry_length": 17, "idx": 15, "format": "s"}}, "module_telemetry": {"0": {"name": "Drive Motor Position [step]", "telemetry_length": 15, "idx": 0, "format": "n"}, "1": {"name": "Drive Motor Angle [deg]", "telemetry_length": 17, "idx": 1, "format": "f"}, "2": {"name": "Drive Status", "telemetry_length": 18, "idx": 2, "format": "x,s,s"}, "3": {"name": "Release Status", "telemetry_length": 18, "idx": 3, "format": "u,s,s"}, "4": {"name": "MPR Status [enabled, mV]", "telemetry_length": 16, "idx": 4, "format": "u,n"}, "5": {"name": "HES Status [enabled, mV]", "telemetry_length": 16, "idx": 5, "format": "u,s"}, "6": {"name": "Temperature 1 [100mK]", "telemetry_length": 15, "idx": 6, "format": "s"}, "7": {"name": "Temperature 2 [100mK]", "telemetry_length": 15, "idx": 7, "format": "s"}, "8": {"name": "Temperature 3 [100mK]", "telemetry_length": 15, "idx": 8, "format": "s"}, "9": {"name": "Temperature 4 [100mK]", "telemetry_length": 15, "idx": 9, "format": "s"}}}}]

Listing available I2C addresses to use on an I2CDriver

.. code-block:: shell

    $ PumQry.py -t i2cdriver -p /dev/ttyUSB5 -l
    0x52 0x53 0x54 0x55 0x57

Pretty-printing the telemetry data from one of the addresses

.. container:: toggle

    .. container:: header

        .. code-block:: bash

            $ PumQry.py -t i2cdriver -p /dev/ttyUSB5 -d 0x53

    .. code-block:: shell


        [
            {
                "version": "PIM (on STM) Rev D fw v1.3.0a & SupMCU Core v1.4.1b",
                "definition": {
                    "name": "PIM",
                    "cmd_name": "PIM",
                    "address": 83,
                    "supmcu_telemetry": {
                        "0": {
                            "name": "Firmware version",
                            "telemetry_length": 77,
                            "idx": 0,
                            "format": "S"
                        },
                        "1": {
                            "name": "SCPI cmds processed",
                            "telemetry_length": 21,
                            "idx": 1,
                            "format": "l"
                        },
                        "2": {
                            "name": "SCPI errs processed",
                            "telemetry_length": 21,
                            "idx": 2,
                            "format": "l"
                        },
                        "3": {
                            "name": "Voltage status (TBD)",
                            "telemetry_length": 21,
                            "idx": 3,
                            "format": "x,x,x,x"
                        },
                        "4": {
                            "name": "SupMCU CPU self tests",
                            "telemetry_length": 35,
                            "idx": 4,
                            "format": "l,l,s,s,s"
                        },
                        "5": {
                            "name": "Elapsed time in seconds",
                            "telemetry_length": 21,
                            "idx": 5,
                            "format": "l"
                        },
                        "6": {
                            "name": "Elapsed context switches",
                            "telemetry_length": 21,
                            "idx": 6,
                            "format": "l"
                        },
                        "7": {
                            "name": "Elapsed idling hooks",
                            "telemetry_length": 21,
                            "idx": 7,
                            "format": "l"
                        },
                        "8": {
                            "name": "MCU load [%]",
                            "telemetry_length": 17,
                            "idx": 8,
                            "format": "f"
                        },
                        "9": {
                            "name": "Module Serial Number",
                            "telemetry_length": 15,
                            "idx": 9,
                            "format": "s"
                        },
                        "10": {
                            "name": "Module I2C Address",
                            "telemetry_length": 14,
                            "idx": 10,
                            "format": "x"
                        },
                        "11": {
                            "name": "Oscillator Tuning Value",
                            "telemetry_length": 14,
                            "idx": 11,
                            "format": "t"
                        },
                        "12": {
                            "name": "Number of NVM Write cycles",
                            "telemetry_length": 15,
                            "idx": 12,
                            "format": "n"
                        },
                        "13": {
                            "name": "Last Processor Reset",
                            "telemetry_length": 15,
                            "idx": 13,
                            "format": "n"
                        },
                        "14": {
                            "name": "Number Telem. Item [sup,mod]",
                            "telemetry_length": 17,
                            "idx": 14,
                            "format": "s,s"
                        },
                        "15": {
                            "name": "SupMCU (CTMU) Temp [0.1K]",
                            "telemetry_length": 17,
                            "idx": 15,
                            "format": "s"
                        }
                    },
                    "module_telemetry": {
                        "0": {
                            "name": "Payload Currents [mA]",
                            "telemetry_length": 21,
                            "idx": 0,
                            "format": "s,s,s,s"
                        },
                        "1": {
                            "name": "Payload Shunt [micro Ohm]",
                            "telemetry_length": 21,
                            "idx": 1,
                            "format": "s,s,s,s"
                        },
                        "2": {
                            "name": "Payload Current limits [mA]",
                            "telemetry_length": 21,
                            "idx": 2,
                            "format": "s,s,s,s"
                        },
                        "3": {
                            "name": "PIM Status Register",
                            "telemetry_length": 14,
                            "idx": 3,
                            "format": "x"
                        },
                        "4": {
                            "name": "Payload Overcurrents [mA]",
                            "telemetry_length": 21,
                            "idx": 4,
                            "format": "s,s,s,s"
                        },
                        "5": {
                            "name": "Payload Voltages [mV]",
                            "telemetry_length": 21,
                            "idx": 5,
                            "format": "s,s,s,s"
                        }
                    }
                }
            }
        ]

\

Saving telemetry data to all detected devices to a file and not printing anything

.. code-block:: shell

    $ PumQry.py -t i2cdriver -p /dev/ttyUSB5 -f telemetry.json -q
