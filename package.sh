#!/usr/bin/env bash

cd putdig-common
python3 setup.py install -f
cd ..
rm -Rf putdig-common
pip install -U -r dev-requirements.txt

if [ -z "$1" ]; then
    pyinstaller --onefile -n PumQry putdig_cli/pumqry.py
    pyinstaller --onefile -n PumGen putdig_cli/pumgen.py
    pyinstaller --onefile -n PumInj putdig_cli/puminj.py
else
    pyinstaller --onefile -n PumQry-$1 putdig_cli/pumqry.py
    pyinstaller --onefile -n PumGen-$1 putdig_cli/pumgen.py
    pyinstaller --onefile -n PumInj-$1 putdig_cli/puminj.py
fi
